﻿using UnityEngine;
using System.Collections;

public class HealthBar : MonoBehaviour
{

    public GameObject Player;

    public HealthScript playerStats;
    public UISlider healthBar;

    // Use this for initialization
    void Start()
    {
        playerStats = Player.GetComponent<HealthScript>();

        if (playerStats == null)
            throw new MissingReferenceException("Can't find playerstats!");

        healthBar = GetComponent<UISlider>();

        if (healthBar == null)
            throw new MissingReferenceException("Can't find timebar!");

        healthBar.sliderValue = playerStats.GameHealth;
    }

    // Update is called once per frame
    void Update()
    {
        healthBar.sliderValue = playerStats.GameHealth;
    }
}
