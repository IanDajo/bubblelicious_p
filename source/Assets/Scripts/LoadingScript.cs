﻿using UnityEngine;
using System.Collections;

public class LoadingScript : MonoBehaviour {
    
    public float Delay = 5;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if (Delay > 0) {
            Delay -= Time.deltaTime;
        }
	    else
        {
            Application.LoadLevel("Menu");
        }
  }
}
