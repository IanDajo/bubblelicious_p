﻿using UnityEngine;

public class HealthScript : MonoBehaviour
{

  public float MaxHealth = 5;

  public int hp = 5;

  public bool isEnemy = true;

  public void Damage(int damageCount)
  {
    hp -= damageCount;

    if (hp <= 0)
    {

      SpecialEffectsHelper.Instance.Explosion(transform.position);
      SoundEffectsHelper.Instance.MakeExplosionSound();


      Destroy(gameObject);
    }
  }

  void OnTriggerEnter2D(Collider2D otherCollider)
  {

    ShotScript shot = otherCollider.gameObject.GetComponent<ShotScript>();
    if (shot != null)
    {

      if (shot.isEnemyShot != isEnemy)
      {
        Damage(shot.damage);


        Destroy(shot.gameObject); 
      }
    }
  }

  public float GameHealth
  {
      get
      {
          return (hp/MaxHealth);
      }
  }

}