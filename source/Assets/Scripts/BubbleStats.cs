﻿using UnityEngine;
using System.Collections;

public class BubbleStats : MonoBehaviour {

    public int NumberOfEnemies;
    public float MaxNumberOfEnemies;

    private UISlider healthBar;

	// Use this for initialization
	void Start () {
        MaxNumberOfEnemies = NumberOfEnemies = GetNumberOfEnemies();

        healthBar = GetComponent<UISlider>();

        if (healthBar == null)
            throw new MissingReferenceException("Can't find timebar!");

        healthBar.sliderValue = BubbleProgress;
	}
	
	// Update is called once per frame
	void Update () {
        

        healthBar.sliderValue = BubbleProgress;
	}

    private int GetNumberOfEnemies()
    {
        GameObject[] enemies = GameObject.FindGameObjectsWithTag("EnemyBubble");
        if (enemies != null)
        {
            return enemies.Length;
        }
        return 0;
    }

    public float BubbleProgress
    {
        get
        {
            return (GetNumberOfEnemies() / MaxNumberOfEnemies);
        }
    }

}
